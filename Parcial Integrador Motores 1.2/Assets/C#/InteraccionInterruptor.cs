using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InteraccionInterruptor : MonoBehaviour
{
    public float distanciaInteraccion = 2f;
    public TextMeshProUGUI textoInteraccion;
    public Light pointLight;
    public Renderer lamparaRenderer;
    public Color colorEmisionEncendido = Color.white;
    public Color colorEmisionApagado = Color.black;

    private bool interruptorActivado = false;
    private int vecesPresionada = 0;

    void Start()
    {
        OcultarTextoInteraccion();
        ApagarPointLight();

        
        if (lamparaRenderer != null)
        {
            Material materialLampara = lamparaRenderer.material;
            materialLampara.SetColor("_EmissionColor", colorEmisionApagado);
        }
    }

    void Update()
    {
        DetectarInterruptor();

        if (Input.GetKeyDown(KeyCode.Q) && interruptorActivado)
        {
            vecesPresionada++;

            if (vecesPresionada == 3)
            {
                DesaparecerEnemigo4();
                ResetContadorPresiones();
            }

            TogglePointLight();
            ToggleEmisionMaterial();
        }
    }

    void DetectarInterruptor()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.forward, out hit, distanciaInteraccion))
        {
            if (hit.collider.CompareTag("Interruptor"))
            {
                MostrarTextoInteraccion();

                if (Input.GetKeyDown(KeyCode.Q))
                {
                    interruptorActivado = true;
                }
            }
        }
        else
        {
            OcultarTextoInteraccion();
            interruptorActivado = false;
        }
    }

    void MostrarTextoInteraccion()
    {
        textoInteraccion.gameObject.SetActive(true);
        textoInteraccion.text = "[Q] para encender y apagar luz";
    }

    void OcultarTextoInteraccion()
    {
        textoInteraccion.gameObject.SetActive(false);
    }

    void TogglePointLight()
    {
        if (pointLight != null)
        {
            if (pointLight.enabled)
            {
                ApagarPointLight();
            }
            else
            {
                EncenderPointLight();
            }
        }
    }

    void EncenderPointLight()
    {
        pointLight.enabled = true;
    }

    void ApagarPointLight()
    {
        pointLight.enabled = false;
    }

    void ToggleEmisionMaterial()
    {
        if (lamparaRenderer != null)
        {
            Material materialLampara = lamparaRenderer.material;

            if (pointLight.enabled)
            {
                materialLampara.SetColor("_EmissionColor", colorEmisionEncendido);
            }
            else
            {
                materialLampara.SetColor("_EmissionColor", colorEmisionApagado);
            }
        }
    }

    void DesaparecerEnemigo4()
    {
        GameObject[] enemigos = GameObject.FindGameObjectsWithTag("enemigo4");

        foreach (GameObject enemigo4 in enemigos)
        {
            if (enemigo4 != null && enemigo4.TryGetComponent<Enemigo4>(out var controlEnemigo))
            {
                controlEnemigo.DesaparecerEnemigo4();
            }
        }
    }

    void ResetContadorPresiones()
    {
        vecesPresionada = 0;
    }
}
