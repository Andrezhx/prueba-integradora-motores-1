using UnityEngine;
using TMPro;

public class Mensajeinicial : MonoBehaviour
{
    public TextMeshProUGUI textoAMostrar;
    public float tiempoVisible = 15f;
    private float tiempoTranscurrido = 0f;

    void Start()
    {
        MostrarTexto();
    }

    void Update()
    {
        tiempoTranscurrido += Time.deltaTime;

        if (tiempoTranscurrido >= tiempoVisible)
        {
            OcultarTexto();
        }
    }

    void MostrarTexto()
    {
        textoAMostrar.enabled = true;
    }

    void OcultarTexto()
    {
        textoAMostrar.enabled = false;
    }
}
