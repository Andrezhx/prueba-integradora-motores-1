using UnityEngine.UI;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    public float velocidadMovimiento = 5.0f;
    public float amortiguamiento = 0.9f;
    public BarraStamina barraDeStamina;
    private Rigidbody rb;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
        rb.freezeRotation = true;

        barraDeStamina = GetComponent<BarraStamina>();
        if (barraDeStamina == null)
        {
            Debug.LogError("El script BarraStamina no está asignado al jugador.");
        }
    }

    void Update()
    {
        ManejarMovimiento();
        ManejarBloqueoCursor();
    }

    void ManejarMovimiento()
    {
        if (Input.GetKey(KeyCode.W) && barraDeStamina.TieneStamina())
        {
            float movimientoHorizontal = Input.GetAxis("Horizontal");
            float movimientoVertical = Input.GetAxis("Vertical");

            Vector3 movimiento = new Vector3(movimientoHorizontal, 0, movimientoVertical).normalized;
            movimiento = transform.TransformDirection(movimiento) * velocidadMovimiento;

            rb.AddForce(movimiento, ForceMode.VelocityChange);
            rb.velocity *= amortiguamiento;

            barraDeStamina.ConsumirStamina(5f * Time.deltaTime);
        }
        else
        {
            rb.velocity = Vector3.zero;
        }
    }

    void ManejarBloqueoCursor()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Cursor.lockState = CursorLockMode.None;
        }
    }
}
