using UnityEngine;
using UnityEngine.UI;

public class BarraStamina : MonoBehaviour
{
    public Slider slider;

    public float staminaMaxima = 100f;
    public float regeneracionStaminaPorSegundo = 5f;

    private float staminaActual;

    void Start()
    {
        if (slider == null)
        {
            Debug.LogError("Asigna la barra de stamina en el Inspector.");
            return;
        }

        staminaActual = staminaMaxima;
        InicializarBarraDeStamina();
    }

    void Update()
    {
        ActualizarBarraDeStamina();
    }

    void InicializarBarraDeStamina()
    {
        slider.maxValue = staminaMaxima;
        slider.value = staminaMaxima;
    }

    void ActualizarBarraDeStamina()
    {
       
        if (!Input.GetKey(KeyCode.W) && staminaActual < staminaMaxima)
        {
            staminaActual += regeneracionStaminaPorSegundo * Time.deltaTime;
        }

        
        staminaActual = Mathf.Clamp(staminaActual, 0f, staminaMaxima); 
        slider.value = staminaActual;
    }

    public void ConsumirStamina(float cantidad)
    {
        
        if (Input.GetKey(KeyCode.W))
        {
            staminaActual -= cantidad;
        }
        staminaActual = Mathf.Clamp(staminaActual, 0f, staminaMaxima);
        slider.value = staminaActual;
    }

    public bool TieneStamina()
    {
        return staminaActual > 0f;
    }
}
