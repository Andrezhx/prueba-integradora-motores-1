using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Armario : MonoBehaviour
{
    public float distanciaDeteccion = 2f;
    public Text textoInArmario;

    private Transform puntoTeletransporteAdentro;
    private Transform puntoTeletransporteAfuera;

    bool enAdentro = false;

    void Start()
    {
        puntoTeletransporteAdentro = GameObject.FindGameObjectWithTag("Adentro").transform;
        puntoTeletransporteAfuera = GameObject.FindGameObjectWithTag("Afuera").transform;
    }

    void Update()
    {
        Ray rayo = new Ray(transform.position, transform.forward);
        RaycastHit hit;

        if (Physics.Raycast(rayo, out hit, distanciaDeteccion) && hit.collider.CompareTag("armario"))
        {
            textoInArmario.text = "[R]";

            if (Input.GetKeyDown(KeyCode.R))
            {
                
                Teletransportar();
            }
        }
        else
        {
            textoInArmario.text = "";
        }
    }

    void Teletransportar()
    {

        if (enAdentro)
        {
            GameObject.FindGameObjectWithTag("Player").transform.position = puntoTeletransporteAfuera.position;
        }
        else
        {
            GameObject.FindGameObjectWithTag("Player").transform.position = puntoTeletransporteAdentro.position;
            DesaparecerEnemigo2();
        }

        enAdentro = !enAdentro;
    }

    void DesaparecerEnemigo2()
    {
        GameObject[] enemigos = GameObject.FindGameObjectsWithTag("enemigo2");

        foreach (GameObject enemigo2 in enemigos)
        {
            if (enemigo2 != null && enemigo2.TryGetComponent<ControlEnemigo2>(out var controlEnemigo))
            {
                controlEnemigo.DesaparecerEnemigo2();
            }
        }
    }
}
