using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puerta : MonoBehaviour
{
    public float interactionDistance;
    public GameObject intText;
    public string doorOpenAnimName, doorCloseAnimName;
    public AudioClip doorOpen, doorClose;

   
    private bool cerrandoPuerta = false;

    
    private Animator doorAnim;

    void Update()
    {
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, interactionDistance))
        {
            if (hit.collider.gameObject.tag == "puerta")
            {
                GameObject doorParent = hit.collider.transform.root.gameObject;
                doorAnim = doorParent.GetComponent<Animator>(); 
                AudioSource doorSound = hit.collider.gameObject.GetComponent<AudioSource>();
                intText.SetActive(true);

                if (Input.GetKeyDown(KeyCode.Q))
                {
                    if (doorAnim.GetCurrentAnimatorStateInfo(0).IsName(doorOpenAnimName))
                    {
                        doorSound.clip = doorClose;
                        doorSound.Play();

                        doorAnim.ResetTrigger("open");
                        doorAnim.SetTrigger("close");

                        
                        cerrandoPuerta = true;
                    }

                    if (doorAnim.GetCurrentAnimatorStateInfo(0).IsName(doorCloseAnimName))
                    {
                        doorSound.clip = doorOpen;
                        doorSound.Play();
                        doorAnim.ResetTrigger("close");
                        doorAnim.SetTrigger("open");
                    }
                }
            }
            else
            {
                intText.SetActive(false);
            }
        }
        else
        {
            intText.SetActive(false);
        }

        
        if (cerrandoPuerta && doorAnim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f)
        {
            cerrandoPuerta = false;

            
            DesaparecerEnemigo3();
        }
    }

    
    void DesaparecerEnemigo3()
    {
        GameObject[] enemigos = GameObject.FindGameObjectsWithTag("enemigo3");

        foreach (GameObject enemigo3 in enemigos)
        {
            if (enemigo3 != null && enemigo3.TryGetComponent<Enemigo3>(out var controlEnemigo3))
            {
                controlEnemigo3.DesaparecerEnemigo3();
            }
        }
    }
}
