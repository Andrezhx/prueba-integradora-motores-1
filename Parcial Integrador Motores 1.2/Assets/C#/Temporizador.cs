using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Temporizador : MonoBehaviour
{
    public float duracionNoche = 240.0f; // Duración de la noche en segundos (4 minutos)
    public Text textoSurvived; 
    public AudioSource victoriaAudio; 
    public Text textoReloj; 

    private float tiempoTranscurrido = 0.0f;
    private bool nocheTerminada = false;
    private bool haGanado = false; 

    void Start()
    {
        textoSurvived.enabled = false; // Oculta el texto al inicio
    }

    void Update()
    {
        if (!nocheTerminada)
        {
            tiempoTranscurrido += Time.deltaTime;

            // Calcula la hora y los minutos
            int horas = Mathf.FloorToInt((tiempoTranscurrido / duracionNoche) * 6) + 12;
            int minutos = Mathf.FloorToInt((tiempoTranscurrido % duracionNoche) / duracionNoche * 60);

            // Muestra la hora en formato 12 horas (opcional)
            string tiempoTexto = string.Format("{0:D2}:{1:D2} AM", horas % 12, minutos);

           
            textoReloj.text = tiempoTexto;

            if (tiempoTranscurrido >= duracionNoche)
            {
                nocheTerminada = true;
                MostrarResultado();
            }
        }
    }

    void MostrarResultado()
    {
      
        textoSurvived.enabled = true;

        if (haGanado && victoriaAudio != null)
        {
            victoriaAudio.Play();
        }

       
        Invoke("CambiarEscena", 3.0f);
    }

   
    public void GanarJuego()
    {
        haGanado = true;
    }

    void CambiarEscena()
    {
        
        SceneManager.LoadScene("Menu inicial");
    }
}
