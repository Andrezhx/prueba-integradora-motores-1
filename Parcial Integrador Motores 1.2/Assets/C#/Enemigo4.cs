using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Enemigo4 : MonoBehaviour
{
    public AudioClip sonidoAparicion;
    public AudioClip sonidoDesaparicion;
    public AudioClip sonidoScreamer;
    public float tiempoAparicion = 50f;
    public float tiempoDesaparicion = 15f;
    public float tiempoEntreReinicios = 50f;
    public Image imagenScreamer;
    public Text textoGameOver;

    private bool visible = false;

    void Start()
    {
        DesactivarEnemigo4();  // Cambiar a DesactivarEnemigo4()
        DesactivarScreamerYGameOver();
        Invoke("AparecerEnemigo", tiempoAparicion);
    }

    void AparecerEnemigo()
    {
        ReproducirSonido(sonidoAparicion);
        ActivarEnemigo();
        visible = true;
        Invoke("MostrarGameOver", tiempoDesaparicion);
    }

    void MostrarGameOver()
    {
        if (visible)
        {
            ReproducirSonido(sonidoScreamer);
            if (imagenScreamer != null) imagenScreamer.enabled = true;
            if (textoGameOver != null) textoGameOver.enabled = true;
            Invoke("CambiarEscenaMainMenu", 3f);
        }
        DesaparecerEnemigo4();  // Cambiar a DesaparecerEnemigo4()
        Invoke("ReiniciarMecanica", tiempoEntreReinicios);
    }

    public void DesaparecerEnemigo4()
    {
        ReproducirSonido(sonidoDesaparicion);
        DesactivarEnemigo4();  
        visible = false;
    }

    void ReiniciarMecanica()
    {
        DesactivarScreamerYGameOver();
        AparecerEnemigo();
    }

    void DesactivarScreamerYGameOver()
    {
        if (imagenScreamer != null) imagenScreamer.enabled = false;
        if (textoGameOver != null) textoGameOver.enabled = false;
    }

    void ReproducirSonido(AudioClip sonido)
    {
        AudioSource.PlayClipAtPoint(sonido, transform.position);
    }

    void ActivarEnemigo()
    {
        gameObject.SetActive(true);
    }

    void DesactivarEnemigo4()
    {
        gameObject.SetActive(false);
    }

    void CambiarEscenaMainMenu()
    {
        SceneManager.LoadScene("Menu inicial");
    }
}
