using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Linternapickup : MonoBehaviour
{
    public GameObject linterna;
    public float distanciaDeteccion = 2f;
    public Text textoInteractivo;

    private bool linternaRecogida = false;

    void Update()
    {
        if (!linternaRecogida)
        {
            Ray rayo = new Ray(transform.position, transform.forward);
            RaycastHit hit;

            if (Physics.Raycast(rayo, out hit, distanciaDeteccion) && hit.collider.CompareTag("Player"))
            {
                textoInteractivo.text = "Presiona 'E' para recoger la linterna y usarla.";

                if (Input.GetKeyDown(KeyCode.E))
                {
                    linterna.SetActive(true);
                    linterna.GetComponent<Linterna>().linternaEnMano = true;
                    linternaRecogida = true;
                    textoInteractivo.text = "";  
                    Destroy(gameObject);
                }
            }
            else
            {
                textoInteractivo.text = "";
            }
        }
    }
}
