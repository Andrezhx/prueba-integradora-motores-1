using UnityEngine;
using UnityEngine.UI;

public class Linterna : MonoBehaviour
{
    public Light luzLinterna;
    public bool activLight = false; 
    public bool linternaEnMano;
    public float tiempoEntrePresiones = 0.5f;
    private int vecesPresionada = 0;
    public float cantBateria = 100;
    public float perdidaBateria = 0.5f;

    [Header("Visuales")]
    public Image pila1;
    public Image pila2;
    public Image pila3;
    public Image pila4;
    public Sprite PilaVacia;
    public Text porcentaje;

    void Start()
    {
        UpdateBatteryVisuals();
        InvokeRepeating("ReduceBattery", 1f, 1f);

        
        luzLinterna.enabled = false;
        activLight = false;
    }

    void Update()
    {
        cantBateria = Mathf.Clamp(cantBateria, 0, 100);

        if (linternaEnMano)
        {
            int valorBateria = (int)cantBateria;
            porcentaje.text = valorBateria.ToString() + "%";

            if (Input.GetKeyDown(KeyCode.E))
            {
                vecesPresionada++;

                if (vecesPresionada == 2)
                {
                    DesaparecerEnemigo();
                }

                Invoke("ResetContadorPresiones", tiempoEntrePresiones);

                activLight = !activLight;

                if (activLight)
                {
                    luzLinterna.enabled = true;
                }
                else
                {
                    luzLinterna.enabled = false;
                }
            }

            if (activLight && cantBateria > 0)
            {
                cantBateria -= perdidaBateria * Time.deltaTime;
                UpdateBatteryVisuals();
            }
        }
    }

    void UpdateBatteryVisuals()
    {
        float batteryLevel = cantBateria / 100f;

        if (batteryLevel >= 0.75f)
        {
            pila1.enabled = true;
            pila2.enabled = false;
            pila3.enabled = false;
            pila4.enabled = false;
        }
        else if (batteryLevel >= 0.50f && batteryLevel < 0.75f)
        {
            pila1.enabled = false;
            pila2.enabled = true;
            pila3.enabled = false;
            pila4.enabled = false;
        }
        else if (batteryLevel >= 0.25f && batteryLevel < 0.50f)
        {
            pila1.enabled = false;
            pila2.enabled = false;
            pila3.enabled = true;
            pila4.enabled = false;
        }
        else
        {
            pila1.enabled = false;
            pila2.enabled = false;
            pila3.enabled = false;
            pila4.enabled = true;
        }

        UpdateLightIntensity();
    }



    void DesaparecerEnemigo()
    {
        GameObject[] enemigos = GameObject.FindGameObjectsWithTag("enemigo");

        foreach (GameObject enemigo in enemigos)
        {
            if (enemigo != null && enemigo.TryGetComponent<ControlEnemigo>(out var controlEnemigo))
            {
                controlEnemigo.DesaparecerEnemigo();
            }
        }

        vecesPresionada = 0;
    }

    void UpdateLightIntensity()
    {
      
        float intensidad = cantBateria / 100f;

        intensidad = Mathf.Clamp01(intensidad);

        float intensidadFinal = 100f * intensidad;

        luzLinterna.intensity = intensidadFinal;
    }

    void ResetContadorPresiones()
    {
        vecesPresionada = 0;
    }

    void ReduceBattery()
    {
        if (activLight && cantBateria > 0)
        {
            cantBateria -= 1f;
            UpdateBatteryVisuals();
        }
    }

    public void RecargarBateria(float cantidad)
    {
        
        if (cantBateria < 100)
        {
            cantBateria += cantidad;

            
            cantBateria = Mathf.Clamp(cantBateria, 0, 100);

           
            UpdateBatteryVisuals();
        }
    }

   
}

