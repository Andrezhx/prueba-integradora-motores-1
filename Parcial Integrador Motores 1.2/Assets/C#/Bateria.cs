using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bateria : MonoBehaviour
{
    public Linterna linterna; 
    public float recargaBateria = 50f; 

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Bateria"))
        {
            
            Destroy(other.gameObject);

            
            GetComponent<Collider>().isTrigger = false;

            
            linterna.RecargarBateria(recargaBateria);
        }
    }
}