using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlEnemigo2 : MonoBehaviour
{
    public AudioClip sonidoAparicion;
    public AudioClip sonidoDesaparicion;
    public AudioClip sonidoScreamer;
    public float tiempoAparicion = 10f;
    public float tiempoDesaparicion = 15f;
    public float tiempoEntreReinicios = 20f;
    public Image imagenScreamer;
    public Text textoGameOver;
    private bool visible = false;

    void Start()
    {
        DesactivarEnemigo();
        DesactivarScreamerYGameOver();
        Invoke("AparecerEnemigo", tiempoAparicion);
    }

    void Update()
    {
      
    }

    void AparecerEnemigo()
    {
        ReproducirSonido(sonidoAparicion);
        ActivarEnemigo();
        visible = true;
        Invoke("MostrarGameOver", tiempoDesaparicion);
    }

    void MostrarGameOver()
    {
        if (visible)
        {
            ReproducirSonido(sonidoScreamer);
            if (imagenScreamer != null) imagenScreamer.enabled = true;
            if (textoGameOver != null) textoGameOver.enabled = true;
            Invoke("CambiarEscenaMainMenu", 3f);
        }
        DesaparecerEnemigo2();
        Invoke("ReiniciarMecanica", tiempoEntreReinicios);
    }

    public void DesaparecerEnemigo2()
    {
        ReproducirSonido(sonidoDesaparicion);
        DesactivarEnemigo();
        visible = false;
    }

    void ReiniciarMecanica()
    {
        DesactivarScreamerYGameOver();
        AparecerEnemigo();
    }

    void DesactivarScreamerYGameOver()
    {
        if (imagenScreamer != null) imagenScreamer.enabled = false;
        if (textoGameOver != null) textoGameOver.enabled = false;
    }

    void ReproducirSonido(AudioClip sonido)
    {
        AudioSource.PlayClipAtPoint(sonido, transform.position);
    }

    void ActivarEnemigo()
    {
        gameObject.SetActive(true);
    }

    void DesactivarEnemigo()
    {
        gameObject.SetActive(false);
    }
    void CambiarEscenaMainMenu()
    {
        SceneManager.LoadScene("Menu inicial");
    }
}
